<?php

namespace App\Repository\Products\CBD;

use App\Entity\Products\CBD\Vaporisator;
use App\Entity\Products\Product;
use App\Repository\Products\ProduitRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Vaporisator>
 *
 * @method Vaporisator|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vaporisator|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vaporisator[]    findAll()
 * @method Vaporisator[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VaporisatorRepository extends ProduitRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vaporisator::class);
    }

    public function add(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return Vaporisator[] Returns an array of Vaporisator objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('v.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Vaporisator
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
