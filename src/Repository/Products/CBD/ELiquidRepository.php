<?php

namespace App\Repository\Products\CBD;

use App\Entity\Products\CBD\ELiquid;
use App\Entity\Products\Product;
use App\Repository\Products\ProduitRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ELiquid>
 *
 * @method ELiquid|null find($id, $lockMode = null, $lockVersion = null)
 * @method ELiquid|null findOneBy(array $criteria, array $orderBy = null)
 * @method ELiquid[]    findAll()
 * @method ELiquid[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ELiquidRepository extends ProduitRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ELiquid::class);
    }

    public function add(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return ELiquid[] Returns an array of ELiquid objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ELiquid
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
