<?php

namespace App\Repository\Products\CBD;

use App\Entity\Products\CBD\Oil;
use App\Entity\Products\Product;
use App\Repository\Products\ProduitRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Oil>
 *
 * @method Oil|null find($id, $lockMode = null, $lockVersion = null)
 * @method Oil|null findOneBy(array $criteria, array $orderBy = null)
 * @method Oil[]    findAll()
 * @method Oil[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OilRepository extends ProduitRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Oil::class);
    }

    public function add(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

}
