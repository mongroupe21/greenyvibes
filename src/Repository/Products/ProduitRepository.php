<?php

namespace App\Repository\Products;

use App\Entity\Products\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Product>
 *
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
abstract class ProduitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, $class)
    {
        parent::__construct($registry, $class);
    }

    public function add(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getFilterQuery($filters): Query
    {
        $qb = $this->createQueryBuilder('o');

        foreach ($filters as $key => $filter){
            $attribute = $filter['attribute'];

            $values = [];
            foreach ($filter['options'] as $option){
                $value = $option['name'];
                $values[] = $value;
            }

            if($attribute === 'brand_id'){
                $qb
                    ->join('o.brand', 'b')
                    ->andWhere("b.name IN (:val$key)")
                    ->setParameter("val$key", $values);
            } else {
                $qb->andWhere("o.$attribute IN (:val$key)");
                $qb->setParameter("val$key", $values);
            }
        }
        $qb->orderBy('o.id', 'DESC');
        return $qb->getQuery();
    }

    public function findAllQuery(): Query
    {
        $qb = $this
            ->createQueryBuilder('p')
            ->orderBy('p.id', 'DESC');
        return $qb->getQuery();
    }

}
