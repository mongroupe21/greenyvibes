<?php

namespace App\Form;

use App\Entity\Brand;
use App\Entity\Products\CBD\Oil;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class OilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type')
            ->add('name')
            ->add('price')
            ->add('score')
            ->add('concentration')
            ->add('contenance')
            ->add('extraction')
            ->add('brand', EntityType::class, [
                "class" => Brand::class,
                'choice_label' => 'name',
            ])
            ->add('imageFile', VichImageType::class, [
            'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Oil::class,
        ]);
    }
}
