<?php

namespace App\EventListener;

use App\Service\Local;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class LocalEventListener
{

    private array $supportedLocals;
    private string $defaultLocal;

    public function __construct(
        $supportedLocals,
        $defaultLocal,
        private TranslatorInterface $translator
    )
    {
        $this->supportedLocals = explode('|', $supportedLocals);
        $this->defaultLocal = $defaultLocal;
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        $multilingualParams = [];
        $localParams = [];
//        dump($request);
        if ($request->attributes->get('_route_params') !== null) {
            foreach ($this->supportedLocals as $supportedLocal) {
                foreach ($request->attributes->get('_route_params') as $key => $value) {
                    if ($key === '_locale') {
                        $value = $supportedLocal;
                    }
//                dump($this->translator->trans($value, [], null, $supportedLocal), $value);
                    $localParams[$key] = $this->translator->trans($value, [], null, $supportedLocal);
                }
                $multilingualParams[$supportedLocal] = $localParams;
            }
            $request->attributes->set('_route_params_trans', $multilingualParams);
        }
    }

    public function onKernelController(ControllerEvent $event): void
    {
    }

}