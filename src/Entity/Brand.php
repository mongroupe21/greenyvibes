<?php

namespace App\Entity;

use App\Entity\Products\CBD\Cosmetic;
use App\Entity\Products\CBD\ELiquid;
use App\Entity\Products\CBD\Food;
use App\Entity\Products\CBD\Gellule;
use App\Entity\Products\CBD\Herb;
use App\Entity\Products\CBD\Infusion;
use App\Entity\Products\CBD\Oil;
use App\Entity\Products\CBD\Vaporisator;
use App\Repository\BrandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BrandRepository::class)]
class Brand
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\OneToMany(mappedBy: 'brand', targetEntity: Oil::class)]
    private Collection $oil;

    #[ORM\OneToMany(mappedBy: 'brand', targetEntity: Herb::class)]
    private Collection $herbs;

    #[ORM\OneToMany(mappedBy: 'brand', targetEntity: ELiquid::class)]
    private Collection $eLiquids;

    #[ORM\OneToMany(mappedBy: 'brand', targetEntity: Cosmetic::class)]
    private Collection $cosmetics;

    #[ORM\OneToMany(mappedBy: 'brand', targetEntity: Infusion::class)]
    private Collection $infusions;

    #[ORM\OneToMany(mappedBy: 'brand', targetEntity: Food::class)]
    private Collection $food;

    #[ORM\OneToMany(mappedBy: 'brand', targetEntity: Vaporisator::class)]
    private Collection $vaporisators;

    #[ORM\OneToMany(mappedBy: 'brand', targetEntity: Gellule::class)]
    private Collection $gellules;

    public function __construct()
    {
        $this->oil = new ArrayCollection();
        $this->herbs = new ArrayCollection();
        $this->eLiquids = new ArrayCollection();
        $this->cosmetics = new ArrayCollection();
        $this->infusions = new ArrayCollection();
        $this->food = new ArrayCollection();
        $this->vaporisators = new ArrayCollection();
        $this->gellules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Oil>
     */
    public function getOil(): Collection
    {
        return $this->oil;
    }

    public function addOil(Oil $oil): self
    {
        if (!$this->oil->contains($oil)) {
            $this->oil[] = $oil;
            $oil->setBrand($this);
        }

        return $this;
    }

    public function removeOil(Oil $oil): self
    {
        if ($this->oil->removeElement($oil)) {
            // set the owning side to null (unless already changed)
            if ($oil->getBrand() === $this) {
                $oil->setBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Herb>
     */
    public function getHerbs(): Collection
    {
        return $this->herbs;
    }

    public function addHerb(Herb $herb): self
    {
        if (!$this->herbs->contains($herb)) {
            $this->herbs[] = $herb;
            $herb->setBrand($this);
        }

        return $this;
    }

    public function removeHerb(Herb $herb): self
    {
        if ($this->herbs->removeElement($herb)) {
            // set the owning side to null (unless already changed)
            if ($herb->getBrand() === $this) {
                $herb->setBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ELiquid>
     */
    public function getELiquids(): Collection
    {
        return $this->eLiquids;
    }

    public function addELiquid(ELiquid $eLiquid): self
    {
        if (!$this->eLiquids->contains($eLiquid)) {
            $this->eLiquids[] = $eLiquid;
            $eLiquid->setBrand($this);
        }

        return $this;
    }

    public function removeELiquid(ELiquid $eLiquid): self
    {
        if ($this->eLiquids->removeElement($eLiquid)) {
            // set the owning side to null (unless already changed)
            if ($eLiquid->getBrand() === $this) {
                $eLiquid->setBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Cosmetic>
     */
    public function getCosmetics(): Collection
    {
        return $this->cosmetics;
    }

    public function addCosmetic(Cosmetic $cosmetic): self
    {
        if (!$this->cosmetics->contains($cosmetic)) {
            $this->cosmetics[] = $cosmetic;
            $cosmetic->setBrand($this);
        }

        return $this;
    }

    public function removeCosmetic(Cosmetic $cosmetic): self
    {
        if ($this->cosmetics->removeElement($cosmetic)) {
            // set the owning side to null (unless already changed)
            if ($cosmetic->getBrand() === $this) {
                $cosmetic->setBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Infusion>
     */
    public function getInfusions(): Collection
    {
        return $this->infusions;
    }

    public function addInfusion(Infusion $infusion): self
    {
        if (!$this->infusions->contains($infusion)) {
            $this->infusions[] = $infusion;
            $infusion->setBrand($this);
        }

        return $this;
    }

    public function removeInfusion(Infusion $infusion): self
    {
        if ($this->infusions->removeElement($infusion)) {
            // set the owning side to null (unless already changed)
            if ($infusion->getBrand() === $this) {
                $infusion->setBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Food>
     */
    public function getFood(): Collection
    {
        return $this->food;
    }

    public function addFood(Food $food): self
    {
        if (!$this->food->contains($food)) {
            $this->food[] = $food;
            $food->setBrand($this);
        }

        return $this;
    }

    public function removeFood(Food $food): self
    {
        if ($this->food->removeElement($food)) {
            // set the owning side to null (unless already changed)
            if ($food->getBrand() === $this) {
                $food->setBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Vaporisator>
     */
    public function getVaporisators(): Collection
    {
        return $this->vaporisators;
    }

    public function addVaporisator(Vaporisator $vaporisator): self
    {
        if (!$this->vaporisators->contains($vaporisator)) {
            $this->vaporisators[] = $vaporisator;
            $vaporisator->setBrand($this);
        }

        return $this;
    }

    public function removeVaporisator(Vaporisator $vaporisator): self
    {
        if ($this->vaporisators->removeElement($vaporisator)) {
            // set the owning side to null (unless already changed)
            if ($vaporisator->getBrand() === $this) {
                $vaporisator->setBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Gellule>
     */
    public function getGellules(): Collection
    {
        return $this->gellules;
    }

    public function addGellule(Gellule $gellule): self
    {
        if (!$this->gellules->contains($gellule)) {
            $this->gellules[] = $gellule;
            $gellule->setBrand($this);
        }

        return $this;
    }

    public function removeGellule(Gellule $gellule): self
    {
        if ($this->gellules->removeElement($gellule)) {
            // set the owning side to null (unless already changed)
            if ($gellule->getBrand() === $this) {
                $gellule->setBrand(null);
            }
        }

        return $this;
    }
}
