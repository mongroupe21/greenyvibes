<?php

namespace App\Controller\Admin;

use App\Entity\Products\CBD\Oil;
use App\Form\OilType;
use App\Repository\Products\CBD\OilRepository;
use App\Service\FormHelper;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/cbd')]
class AdminCategoryController extends AbstractController
{
    #[Route('/oil', name: 'admin.index.oil')]
    public function index(OilRepository $repository, PaginatorInterface $paginator, Request $request): Response
    {
        $query = $repository->findAllQuery();
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            24
        );
        return $this->render('admin/categories/cbd/index-oil.html.twig', [
            'products' => $pagination->getItems(),
            'pagination' => $pagination
        ]);
    }

    #[Route('/oil/edit/{id}', name: 'admin.edit.oil', methods: 'GET|POST')]
    public function edit(Request $request, EntityManagerInterface $em, Oil $oil): Response
    {
        $form = $this->createForm(OilType::class, $oil);
        $formHelper = new FormHelper($form, $request, $em);
        if ($formHelper->submitWithNoErrors()) {
            $formHelper->sendDB();
            return $this->redirectToRoute('admin.index.oil');
        }
        return $this->render('admin/categories/cbd/edit-oil.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/oil/create', name: 'admin.create.oil')]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $oil = new Oil();
        $form = $this->createForm(OilType::class, $oil);
        $formHelper = new FormHelper($form, $request, $em);
        if ($formHelper->submitWithNoErrors()) {
            $formHelper->sendDB();
            return $this->redirectToRoute('admin.index.oil');
        }
        return $this->render('admin/categories/cbd/edit-oil.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/oil/delete/{id}', name: 'admin.delete.oil', methods: 'DELETE')]
    public function delete(EntityManagerInterface $em, Oil $oil, Request $request): Response {
        if($this->isCsrfTokenValid('delete' . $oil->getId(), $request->get('_token') )) {
            $em->remove($oil);
            $em->flush();
        }
            return $this->redirectToRoute('admin.index.oil');
    }

}
