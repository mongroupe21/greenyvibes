<?php

namespace App\Controller;


use App\Service\Local;
use App\Repository\Products\CBD\OilRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route( path: '/{_locale}/', name: 'home', requirements: ['_locale' => 'en|fr'])]
    public function index(): Response
    {
        return $this->render('home.html.twig');
    }

    #[Route('/')]
    public function indexNoLocale(Request $request, $supportedLocals, $defaultLocal): Response
    {
        $local = new Local($request, $supportedLocals, $defaultLocal);
        $local = $local->getLocal();
        return $this->redirectToRoute('home', ['_locale' => $local]);
    }
}
