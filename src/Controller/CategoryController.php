<?php

namespace App\Controller;

use App\DataFixtures\CategoryFixtures;
use App\Repository\CategoryRepository;
use App\Repository\Products\CBD\CosmeticRepository;
use App\Repository\Products\CBD\ELiquidRepository;
use App\Repository\Products\CBD\FoodRepository;
use App\Repository\Products\CBD\GelluleRepository;
use App\Repository\Products\CBD\HerbRepository;
use App\Repository\Products\CBD\InfusionRepository;
use App\Repository\Products\CBD\OilRepository;
use App\Repository\Products\CBD\VaporisatorRepository;
use App\Service\Filter;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route( path: '{_locale<%app.supported_locals%>}/{category}')]
class CategoryController extends AbstractController
{
    public function __construct(
        private readonly PaginatorInterface $paginator,
        private readonly Filter $filter
    ){
        $get = $_GET;
        unset($get['page']);
        $this->filter->setActiveFilters($get);
    }

    #[Route('/{subcategory}', name: 'category.index')]
    public function oils(Request $request , $category, $subcategory, TranslatorInterface $translator, ManagerRegistry $registry): Response
    {
        $defaultLocal = $this->getParameter('app.default_local');
        $file_subcategory = $translator->trans($subcategory, [], null, $defaultLocal);
        $file_category = $translator->trans($category, [], null, $defaultLocal);
        $view = "categories\\$file_category\\$file_subcategory.html.twig";
//        dd($view);
        $entityName = "App\\Entity\\Products\\CBD\\".ucwords($file_subcategory);
        $entity = new $entityName;
        // Instancier le bon repository
        $repository = $registry->getRepository($entity::class);
        $this->filter->setFilters(['slug' => $file_subcategory]);

        if(empty($this->filter->getActiveFilters())) {
            $query = $repository->findAllQuery();
        } else {
            $this->filter->updateFilters();
            $query = $repository->getFilterQuery($this->filter->getQueryFilters());
        }
        $pagination = $this->getPagination($query, $request);

        return $this->render($view, [
            'filters' => $this->filter->getFilters(),
            'products' => $pagination->getItems(),
            'pagination' => $pagination,
            'category' => $file_subcategory
        ]);
    }

    private function getPagination($query, $request): PaginationInterface
    {
        return $this->paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            12 /*limit per page*/
        );
    }

//    #[Route('/herb', name: 'category.index.herbs')]
//    public function herbs(HerbRepository $herbRepository): Response
//    {
//        $category = $this->categoryRepository->findOneBy(["slug" => 'herbe']);
//        $products = $herbRepository->findAll();
//        $result = $this->categories($category, $products, $herbRepository);
//
//
//        return $this->render('categories\cbd\herb.html.twig', [
//            'filters' => $result['filters'],
//            'products' => $result['Products'],
//            'no_result' => $result['noResult'],
//        ]);
//    }
//
//    #[Route('/eliquid', name: 'category.index.eliquids')]
//    public function eliquids(ELiquidRepository $eLiquidRepository): Response
//    {
//        $category = $this->categoryRepository->findOneBy(["slug" => 'e-liquid']);
//        $products = $eLiquidRepository->findAll();
//        $result = $this->categories($category, $products, $eLiquidRepository);
//
//        return $this->render('categories\cbd\eliquids.html.twig', [
//            'filters' => $result['filters'],
//            'products' => $result['Products'],
//            'no_result' => $result['noResult'],
//        ]);
//    }
//
//    #[Route('/cosmetic', name: 'category.index.cosmetics')]
//    public function cosmetics(CosmeticRepository $cosmeticRepository): Response
//    {
//        $category = $this->categoryRepository->findOneBy(["slug" => 'cosmetique']);
//        $products = $cosmeticRepository->findAll();
//        $result = $this->categories($category, $products, $cosmeticRepository);
//
//        return $this->render('categories\cbd\cosmetics.html.twig', [
//            'filters' => $result['filters'],
//            'products' => $result['Products'],
//            'no_result' => $result['noResult'],
//        ]);
//    }
//
//    #[Route('/infusion', name: 'category.index.infusions')]
//    public function infusions(InfusionRepository $infusionRepository): Response
//    {
//        $category = $this->categoryRepository->findOneBy(["slug" => 'infusion']);
//        $products = $infusionRepository->findAll();
//        $result = $this->categories($category, $products, $infusionRepository);
//
//        return $this->render('categories\cbd\infusions.html.twig', [
//            'filters' => $result['filters'],
//            'products' => $result['Products'],
//            'no_result' => $result['noResult'],
//        ]);
//    }
//
//    #[Route('/food', name: 'category.index.food')]
//    public function food(FoodRepository $foodRepository): Response
//    {
//        $category = $this->categoryRepository->findOneBy(["slug" => 'nourriture']);
//        $products = $foodRepository->findAll();
//        $result = $this->categories($category, $products, $foodRepository);
//
//        return $this->render('categories\cbd\food.html.twig', [
//            'filters' => $result['filters'],
//            'products' => $result['Products'],
//            'no_result' => $result['noResult'],
//        ]);
//    }
//
//    #[Route('/vaporisator', name: 'category.index.vaporisators')]
//    public function vaporisators(VaporisatorRepository $vaporisatorRepository): Response
//    {
//        $category = $this->categoryRepository->findOneBy(["slug" => 'vaporisateur']);
//        $products = $vaporisatorRepository->findAll();
//        $result = $this->categories($category, $products, $vaporisatorRepository);
//
//        return $this->render('categories\cbd\vaporisators.html.twig', [
//            'filters' => $result['filters'],
//            'products' => $result['Products'],
//            'no_result' => $result['noResult'],
//        ]);
//    }
//
//    #[Route('/gelule', name: 'category.index.gelules')]
//    public function gelules(GelluleRepository $gelluleRepository): Response
//    {
//        $category = $this->categoryRepository->findOneBy(["slug" => 'gelules']);
//        $products = $gelluleRepository->findAll();
//        $result = $this->categories($category, $products, $gelluleRepository);
//
//        return $this->render('categories\cbd\gelules.html.twig', [
//            'filters' => $result['filters'],
//            'products' => $result['Products'],
//            'no_result' => $result['noResult'],
//        ]);
//    }

    #[Route('/test', name: 'test')]
    public function test(CategoryFixtures $fixture): Response
    {
        return $this->render('test.html.twig');
    }
}
