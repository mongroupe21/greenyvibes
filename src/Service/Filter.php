<?php

namespace App\Service;

use App\Repository\CategoryRepository;

/*
  * Pour fonctionner en autonomie cette classe à besoin de se voir défini via des setters au sein du controller
  * activeFilters, filters
  * A partir de ça on peut déduire filterValues et queryFilter
*/

class Filter {

    public function __construct(
        private readonly CategoryRepository $repository,
    )
    {}

    /**
     * @var array
     * Correspond aux filtres associés à la page catégorie actuelle qui sert à l'affichage
     */
    private array $filters;

    /**
     * @var array
     * Correspond aux filtres sélectionnés par l'utilisateur et passé dans $_GET
     */
    private array $activeFilters;

    /**
     * @var array
     * Correspond à l'ensemble des valeurs associés aux filtres sélectionnés par l'utilisateur
     */
    private array $filterValues;

    /**
     * @var array
     * Correspond aux filtres associés à la catégorie actuelle qui sont actuellement actif.
     * Comprend les valeurs, et au bon format, qui permettent de réaliser la requête
     */

    private array $queryFilters;

    /**
     * @var CategoryRepository
     * Correspond au repository de l'entité Catégorie
     * Comprend les valeurs, et au bon format, qui permettent de réaliser la requête
     */

    public function getRepository(): CategoryRepository
    {
        return $this->repository;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }

    public function setFilters(array $category_slug): Filter
    {
        $filters = $this->getRepository()->findOneBy($category_slug)->getFilters();
        $this->filters = $filters;
        return $this;
    }

    public function getActiveFilters(): array
    {
        return $this->activeFilters;
    }


    public function setActiveFilters(array $activeFilters): Filter
    {
        $this->activeFilters = $activeFilters;
        return $this;
    }

    public function setFilterValues(): Filter
    {
        $filterValues = [];
        foreach ($this->activeFilters as $activeFilter) {
            foreach ($activeFilter as $filterValue) {
                $filterValues[] = $filterValue;
            }
        }
        $this->filterValues = $filterValues;
        return $this;
    }

    public function getQueryFilters(): array
    {
        return $this->queryFilters;
    }

    public function updateFilters(): void
    {
        $this->setFilterValues();
        $queryFilters = [];
        foreach ($this->filters as $keyFilter => $filter) {
            if (array_key_exists($filter['slug'], $this->activeFilters)) {
                $this->filters[$keyFilter]['checked'] = true;
                foreach ($filter['options'] as $keyOption => $option) {
                    if (!in_array($option['slug'], $this->filterValues)) {
                        unset($filter['options'][$keyOption]);
                    } else {
                        $this->filters[$keyFilter]['options'][$keyOption]['checked'] = true;
                    }
                }
                $queryFilters[] = $filter;
            }
        }
        $this->queryFilters = $queryFilters;
    }
}