<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class FormHelper {

    private FormInterface $form;

    private Request $request;

    private EntityManagerInterface $em;

    public function __construct(FormInterface $form, Request $request, EntityManagerInterface $em )
    {
        $this->form = $form;
        $this->request = $request;
        $this->em = $em;
    }

    public function sendDB(): void
    {
        $data = $this->form->getData();
        $this->em->persist($data);
        $this->em->flush();
    }

    public function submitWithNoErrors(): bool
    {
        $this->form->handleRequest($this->request);
        if ($this->form->isSubmitted() && $this->form->isValid()) {
            return true;
        }
        return false;
    }
}