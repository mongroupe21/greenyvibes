<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;

class Local
{
    private string $userPreferedLocal;
    private array $supportedLocals;
    private string $defaultLocal;
    private bool $matchLocal = false;

    public function __construct(
        $request,
        $supportedLocals,
        $defaultLocal
    )
    {
        $this->userPreferedLocal = substr($request->getPreferredLanguage(), 0, 2);
        $this->supportedLocals = explode('|', $supportedLocals);
        $this->defaultLocal = $defaultLocal;
    }

    private function match(): void
    {
        foreach ($this->supportedLocals as $supportedLocal) {
            if ($supportedLocal === $this->userPreferedLocal) {
                $this->matchLocal = true;
                break;
            }
        }
    }

    public function getLocal(): string
    {
        $this->match();
        if($this->matchLocal) {
            return $this->userPreferedLocal;
        } else {
            return $this->defaultLocal;
        }
    }
}