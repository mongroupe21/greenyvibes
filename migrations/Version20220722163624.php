<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220722163624 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE cosmetic_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE eliquid_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE food_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE gellule_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE herb_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE infusion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE vaporisator_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE cosmetic (id INT NOT NULL, brand_id INT NOT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, score VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, concentration VARCHAR(255) NOT NULL, contenance VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9528AF1F44F5D008 ON cosmetic (brand_id)');
        $this->addSql('CREATE TABLE eliquid (id INT NOT NULL, brand_id INT NOT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, score VARCHAR(255) NOT NULL, concentration VARCHAR(255) NOT NULL, contenance VARCHAR(255) NOT NULL, savor VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D0AF7ECC44F5D008 ON eliquid (brand_id)');
        $this->addSql('CREATE TABLE food (id INT NOT NULL, brand_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, score VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D43829F744F5D008 ON food (brand_id)');
        $this->addSql('CREATE TABLE gellule (id INT NOT NULL, brand_id INT NOT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, score VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, concentration VARCHAR(255) NOT NULL, extraction VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DCFACC44F5D008 ON gellule (brand_id)');
        $this->addSql('CREATE TABLE herb (id INT NOT NULL, brand_id INT NOT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, score VARCHAR(255) NOT NULL, culture VARCHAR(255) NOT NULL, variety VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, savor VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2F7F123B44F5D008 ON herb (brand_id)');
        $this->addSql('CREATE TABLE infusion (id INT NOT NULL, brand_id INT NOT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, score VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, savor VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E873145344F5D008 ON infusion (brand_id)');
        $this->addSql('CREATE TABLE vaporisator (id INT NOT NULL, brand_id INT NOT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, score VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DC2F05CD44F5D008 ON vaporisator (brand_id)');
        $this->addSql('ALTER TABLE cosmetic ADD CONSTRAINT FK_9528AF1F44F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE eliquid ADD CONSTRAINT FK_D0AF7ECC44F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE food ADD CONSTRAINT FK_D43829F744F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE gellule ADD CONSTRAINT FK_DCFACC44F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE herb ADD CONSTRAINT FK_2F7F123B44F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE infusion ADD CONSTRAINT FK_E873145344F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE vaporisator ADD CONSTRAINT FK_DC2F05CD44F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE cosmetic_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE eliquid_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE food_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE gellule_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE herb_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE infusion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE vaporisator_id_seq CASCADE');
        $this->addSql('DROP TABLE cosmetic');
        $this->addSql('DROP TABLE eliquid');
        $this->addSql('DROP TABLE food');
        $this->addSql('DROP TABLE gellule');
        $this->addSql('DROP TABLE herb');
        $this->addSql('DROP TABLE infusion');
        $this->addSql('DROP TABLE vaporisator');
    }
}
