<?php
namespace Deployer;

require 'recipe/symfony.php';

// Config

set('repository', 'git@gitlab.com:mongroupe21/greenyvibes.git');

add('shared_files', []);
add('shared_dirs', ['/public/images']);
add('writable_dirs', []);

// Hosts

host('206.189.63.26')
    ->set('remote_user', 'gregquer')
    ->set('deploy_path', '~/greenyvibes');

task('deploy:config:prod', function () {
    run('cd {{release_path}} && echo "APP_ENV=prod" >> .env.local');
    run('cd {{release_path}} && echo "DATABASE_URL=postgresql://postgres:ephelantrole99@127.0.0.1:5432/greenyvibes-symfony?serverVersion=14&charset=utf8" >> .env.local');
});

task('deploy:nodes', function () {
    run('cd {{release_path}} && yarn && yarn build');
});

//task('deploy:migration', function () {
//   run('cd {{release_path}} && php bin/console --no-interaction d:m:m');
//});

// Hooks

before('deploy:vendors', 'deploy:config:prod');
after('deploy:failed', 'deploy:unlock');
after('deploy:vendors', 'deploy:nodes');
//before('deploy:symlink', 'deploy:migration');
