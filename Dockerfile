FROM php:8.1-fpm AS app-php
#ENV APP_ENV=prod

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    git \
    nano \
    libxml2-dev \
    sudo \
    zip \
    unzip \
    && docker-php-ext-install intl opcache

RUN curl -sSLf \
        -o /usr/local/bin/install-php-extensions \
        https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions && \
    chmod +x /usr/local/bin/install-php-extensions

RUN install-php-extensions pgsql pdo_pgsql gd

COPY ./docker/php/php.ini /usr/local/etc/php/php.ini

RUN pecl install xdebug && docker-php-ext-enable xdebug

COPY ./docker/php/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN chmod +x /usr/bin/composer
ENV COMPOSER_ALLOW_SUPERUSER 1

RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | sudo -E bash && \
    apt install symfony-cli

RUN curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash - && \
    apt install nodejs

RUN npm install --global yarn

RUN mkdir -p /var/www/html

WORKDIR /var/www/html

COPY . .

RUN composer install

RUN yarn install && yarn build

RUN chmod -R 777 var

FROM nginx:1.23.1 AS app-nginx
ENV APP_ENV=prod

RUN rm -rf /etc/nginx/conf.d/*

COPY ./docker/nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=app-php /var/www/html/public /var/www/html/public

FROM postgres:14 AS app-postgres


WORKDIR /var/www/html

COPY . .

FROM postgres:14 AS app-postgres



